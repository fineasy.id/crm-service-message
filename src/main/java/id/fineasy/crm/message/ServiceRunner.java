package id.fineasy.crm.message;

import id.fineasy.crm.message.worker.CampaignScheduler;
import id.fineasy.crm.message.worker.Workers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.lang.invoke.MethodHandles;

@Component
public class ServiceRunner implements ApplicationRunner {
    @Autowired
    Workers workers;

    @Autowired
    private ApplicationContext context;

    @Autowired
    private CampaignScheduler campaignScheduler;

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Override
    public void run(ApplicationArguments applicationArguments) throws Exception {
        log.info("Start runner!");
        workers.start(context);
        campaignScheduler.start();
    }

}
