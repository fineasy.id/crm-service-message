package id.fineasy.crm.message.worker;

import org.apache.commons.lang.text.StrSubstitutor;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Component;

import java.lang.invoke.MethodHandles;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static id.fineasy.crm.message.config.ActiveMQConfig.QUEUE_SMS;

@Component
public class CampaignScheduler {

    @Autowired
    ThreadPoolTaskScheduler taskScheduler;
    @Autowired
    NamedParameterJdbcTemplate namedJdbcTemplate;
    @Autowired
    MessageSender messageSender;

    ApplicationContext context;
    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    public void start() {
        taskScheduler.scheduleWithFixedDelay(new CampaignSchedulerWorker(), 60000);
    }

    private class CampaignSchedulerWorker implements Runnable {
        @Override
        public void run() {
            log.debug("Retriving any due schedule!");
            getCampaigns();
        }

        private void getCampaigns() {
            Calendar cal = Calendar.getInstance();
            int h = cal.get(Calendar.HOUR_OF_DAY);
            int m = cal.get(Calendar.MINUTE);
            int d = cal.get(Calendar.DAY_OF_WEEK);

            log.info("Retrieving schedule: DOW: "+d+", HOUR: "+h+", MINUTE: "+m+" SUNDAY: "+Calendar.SUNDAY+", MONDAY: "+Calendar.MONDAY+", TUE: "+Calendar.TUESDAY+", SAT: "+Calendar.SATURDAY);

            String sql = " SELECT c.*,t.content, t.tag FROM sms_campaign c " +
                    " JOIN sms_campaign_schedule s ON (c.id=s.campaign_id) " +
                    " JOIN sms_template t ON (c.template_id=t.id) " +
                    " WHERE (sch_dow IS NULL OR sch_dow=:d) AND (sch_hour IS NULL OR sch_hour=:h) AND (sch_min IS NULL OR sch_min=:m) ";

            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue("h", h);
            params.addValue("d", d);
            params.addValue("m", m);

            List<Map<String,Object>> campaigns = namedJdbcTemplate.queryForList(sql, params);

            for (Map<String,Object> campaign: campaigns) {
                processCampaign(campaign);
            }
        }

        private void processCampaign(Map<String,Object> campaign) {
            String sql = ""+campaign.get("query");
            String template = ""+campaign.get("content");
            log.info("Processing campaign: "+campaign.get("name")+", query: "+sql);

            MapSqlParameterSource params = new MapSqlParameterSource();

            List<Map<String,Object>> dests = namedJdbcTemplate.queryForList(sql, params);
            for (Map<String,Object> dest:dests) {
                StrSubstitutor substitutor = new StrSubstitutor(dest);
                String content = substitutor.replace(template);
                String mobile = ""+dest.get("mobile");
                log.info("Sending to :"+mobile+", content: "+content);

                JSONObject jsonReq = new JSONObject();
                jsonReq.put("content", content);
                jsonReq.put("mobile", mobile);
                jsonReq.put("tag", campaign.get("tag"));
                jsonReq.put("ref", UUID.randomUUID().toString());

                messageSender.send(QUEUE_SMS, jsonReq);
            }
        }
    }

}
