package id.fineasy.crm.message.worker;

import id.fineasy.crm.message.message.SMS;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;
import java.lang.invoke.MethodHandles;

import static id.fineasy.crm.message.config.ActiveMQConfig.QUEUE_SMS;

@Service
public class MessageSender {
    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Autowired
    private JmsTemplate jmsTemplate;

    public void sendSms(SMS sms) {
        log.info("sending with convertAndSend() to queue <" + sms.toString() + ">");
        jmsTemplate.convertAndSend(QUEUE_SMS, sms.toString());
    }

    public void send(String destination, JSONObject jsonReq) {
        jmsTemplate.convertAndSend(destination, jsonReq.toString());
    }
}
