package id.fineasy.crm.message.worker;

import id.fineasy.crm.message.component.ConfigUtils;
import id.fineasy.crm.message.component.DataUtils;
import id.fineasy.crm.message.component.HttpUtils;
import id.fineasy.crm.message.message.SMS;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Component;
import javax.jms.Session;

import java.lang.invoke.MethodHandles;

import static id.fineasy.crm.message.config.ActiveMQConfig.QUEUE_SMS;

@Component
public class QueueSms {
    @Autowired
    NamedParameterJdbcTemplate namedJdbcTemplate;
    @Autowired
    ThreadPoolTaskExecutor asyncExecutor;
    @Autowired
    ThreadPoolTaskScheduler taskScheduler;
    @Autowired
    DataUtils dataUtils;
    @Autowired
    DataSourceTransactionManager trxManager;
    @Autowired
    ConfigUtils configUtils;

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @JmsListener(destination = QUEUE_SMS)
    public void receiveAndSend(@Payload String jsonString, @Headers MessageHeaders headers, Message message, Session session) {

        try {
            JSONObject jsonReq = new JSONObject(jsonString);
            log.info("Received message: "+jsonReq.toString());
            asyncExecutor.execute(new SmsSenderWorker(jsonReq));
        } catch (Exception e) {
            log.error("Error while receiving message: "+e,e);
        }
    }

    private class SmsSenderWorker implements Runnable {
        private final JSONObject jsonReq;
        public SmsSenderWorker(JSONObject jsonReq) {
            this.jsonReq = jsonReq;
        }

        @Override
        public void run() {
            try {
                sendSms(jsonReq);
            } catch (Exception e) {
                log.error("Error sending SMS: "+e,e);
            }
        }
    }

    private void sendSms(JSONObject jsonReq) throws Exception {

        if (configUtils.getConfig("sms.enabled").equals("0")) {
            log.info("SMS sending is disabled!");
            return;
        }

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("mobile", jsonReq.get("mobile"));
        params.addValue("tag", jsonReq.get("tag"));
        params.addValue("ref", jsonReq.get("ref"));
        params.addValue("content", jsonReq.get("content"));

        String sql = " INSERT INTO sms_sent (mobile,content,tag,ref) VALUES (:mobile,:content,:tag,:ref) ";
        KeyHolder holder = new GeneratedKeyHolder();
        namedJdbcTemplate.update(sql, params, holder);
        long smsId = holder.getKey().longValue();
        params.addValue("smsId", smsId);
        log.info("Inserted sms ID#"+smsId);

        try {
            JSONObject jsonResult = send2(jsonReq.getString("mobile"), jsonReq.getString("content"));
            params.addValue("success", jsonResult.get("success"));
            params.addValue("sentStatus", jsonResult.get("statusMessage"));
            params.addValue("msgId", (jsonResult.has("msgId")?jsonResult.get("msgId"):null));
        } catch (Exception e) {
            log.error("Error sending SMS: "+e);
            params.addValue("success", 0);
            params.addValue("sentStatus", e.getMessage());
        }
        finally {
            sql = " UPDATE sms_sent SET sent_on=NOW(), success=:success, sent_status=:sentStatus,msg_id=:msgId WHERE id=:smsId ";
            namedJdbcTemplate.update(sql, params);
        }
    }

    public JSONObject send1(String mobile, String content) throws Exception {
        // example ... http://10.1.1.22:8080/sms/send.json?msisdn=08988883636&text=test&key=1ef1d1cb35fa396208905134ef1cb428
        URIBuilder urlBuilder = new URIBuilder(configUtils.getConfig("sms.gateway.modem.url"));
        urlBuilder.setParameter("key", "1ef1d1cb35fa396208905134ef1cb428");
        urlBuilder.setParameter("msisdn", mobile);
        urlBuilder.setParameter("text", content);

        String sendUrl = urlBuilder.build().toString();
        log.info("URL: "+sendUrl);
        HttpGet httpget = new HttpGet(sendUrl);
        CloseableHttpResponse response = HttpUtils.getHttpClient().execute(httpget);
        HttpEntity entity = response.getEntity();

        byte[] bytes = EntityUtils.toByteArray(entity);
        String respText = new String(bytes);

        JSONObject jsonResult = new JSONObject(respText);
        log.info("Response: "+jsonResult.toString(4));
        return jsonResult;
    }

    public JSONObject send2(String mobile, String content) throws Exception {
        String url = configUtils.getConfig("sms.infobip.url");
        String auth = configUtils.getConfig("sms.infobip.auth");
        String from = configUtils.getConfig("sms.infobip.from");

        log.info("Post JSON to: "+url);
        HttpPost httpPost = new HttpPost(url);

        JSONObject jsonReq = new JSONObject();
        jsonReq.put("from", from);
        jsonReq.put("to", mobile);
        jsonReq.put("text", content.trim());

        httpPost.setHeader("Content-Type", "application/json");
        httpPost.setHeader("Authorization", auth);
        httpPost.setEntity(new StringEntity(jsonReq.toString(4)));

        CloseableHttpResponse response = HttpUtils.getHttpClient().execute(httpPost);

        HttpEntity entity = response.getEntity();
        byte[] bytes = EntityUtils.toByteArray(entity);
        String respText = new String(bytes);
        log.info("Post JSON response: "+respText);

        JSONObject jsonResultRaw = new JSONObject(respText);
        String statusName = jsonResultRaw.getJSONArray("messages").getJSONObject(0).getJSONObject("status").getString("name");
        log.info("Result: "+statusName);

        JSONObject jsonResult = new JSONObject();
        jsonResult.put("statusMessage", statusName);
        jsonResult.put("success", statusName.startsWith("PENDING"));
        String msgId = jsonResultRaw.getJSONArray("messages").getJSONObject(0).getString("messageId");
        jsonResult.put("msgId", msgId);

        return jsonResult;
    }
}
