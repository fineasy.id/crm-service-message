package id.fineasy.crm.message.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class DataUtils {
    @Autowired
    NamedParameterJdbcTemplate namedJdbcTemplate;

    public final static int USER_ID_SYSTEM = 1;
    public static final int STATUS_NEW = 251;
    public static final int STATUS_REJECTED_MOBILE_EMPTY = 259;
    public static final int STATUS_GATE_UNEXPECTED_ERROR = 991;
    public static final int STATUS_GATE_PASSED_VERIFICATION = 252;
    public static final int STATUS_GATE_PASSED_TELESALES = 253;
    public static final int STATUS_GATE_REJECTED_ALLOWED_MINIMUM_REAPPLY_DAYS = 256;
    public static final int STATUS_GATE_REJECTED_DUPLICATE_ACTIVE_APPLICATION = 257;
    public static final int STATUS_GATE_REJECTED_DUPLICATE_ACTIVE_AGREEMENT = 258;


    public static final int TELESALES_SOURCE_TYPE_INCOMPLETE_APPLICATION = 1;
    public static final int TELESALES_SOURCE_TYPE_ELIGIBLE_REPEAT = 2;
    public static final int TELESALES_SOURCE_TYPE_EXTERNAL_DATA = 3;

    public static final int STATUS_CONTRACT_SIGNED = 101;

    public Map<String,Object> getTmpApplication(long tmpId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("tmpId", tmpId);
        String sql = " SELECT * FROM _application_gate WHERE tmp_id=:tmpId ";
        return namedJdbcTemplate.queryForMap(sql, params);
    }

    public void updateTmpApplicationStatus(long tmpId, int statusId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("tmpId", tmpId);
        params.addValue("statusId", statusId);

        String sql = " UPDATE application_tmp SET status_id=:statusId WHERE tmp_id=:tmpId ";
        namedJdbcTemplate.update(sql, params);
    }

    public void updateTmpApplicationStatusAndProcessed(long tmpId, int statusId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("tmpId", tmpId);
        params.addValue("statusId", statusId);

        String sql = " UPDATE application_tmp SET status_id=:statusId, is_processed=1, processed_on=NOW() WHERE tmp_id=:tmpId ";
        namedJdbcTemplate.update(sql, params);
    }
}
