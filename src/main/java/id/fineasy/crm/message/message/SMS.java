package id.fineasy.crm.message.message;

import org.json.JSONObject;

import java.io.Serializable;

public class SMS {
    private final String mobile;
    private final String content;
    private final String tag;
    private final String ref;


    public SMS(String mobile, String content, String tag, String ref) {
        this.mobile = mobile;
        this.content = content;
        this.tag = tag;
        this.ref = ref;
    }

    public SMS(String jsonString) {
        JSONObject jsonObject = new JSONObject(jsonString);
        mobile = jsonObject.getString("mobile");
        content = jsonObject.getString("content");
        tag = jsonObject.getString("tag");
        ref = jsonObject.getString("ref");
    }


    public String getContent() {
        return content;
    }

    public String getMobile() {
        return mobile;
    }

    public String getTag() {
        return tag;
    }

    public String getRef() {
        return this.ref;
    }


    @Override
    public String toString() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("mobile", mobile);
        jsonObject.put("content", content);
        jsonObject.put("tag", tag);
        jsonObject.put("ref", ref);
        return jsonObject.toString(4);
    }
}
