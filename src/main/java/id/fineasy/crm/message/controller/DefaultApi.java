package id.fineasy.crm.message.controller;

import id.fineasy.crm.message.message.SMS;
import id.fineasy.crm.message.worker.MessageSender;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.UUID;

import static id.fineasy.crm.message.config.ActiveMQConfig.QUEUE_SMS;

@RestController
public class DefaultApi {

    @Autowired
    MessageSender messageSender;

    @RequestMapping(value = {"/sms/send"}, produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> restApi(HttpServletRequest request) {
        JSONObject jsonResult = new JSONObject();
        HttpStatus httpStatus = HttpStatus.OK;

        String mobile = request.getParameter("mobile");
        if (mobile.startsWith("0")) mobile = mobile.replaceFirst("0","+62");
        else if (mobile.startsWith("62")) mobile.replaceFirst("62", "+62");
        String content = request.getParameter("content");
        String ref = (request.getParameter("ref")==null)? UUID.randomUUID().toString():request.getParameter("ref");
        String tag = request.getParameter("tag");

        SMS sms = new SMS(mobile, content, tag, ref);

        JSONObject jsonReq = new JSONObject();
        jsonReq.put("mobile", mobile);
        jsonReq.put("content", content);
        jsonReq.put("ref", ref);
        jsonReq.put("tag", (tag==null)?"UNKNOWN":tag);
        messageSender.send(QUEUE_SMS, jsonReq);
        //messageSender.sendSms(sms);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        return new ResponseEntity<String>(jsonResult.toString(), headers, httpStatus);

    }
}
